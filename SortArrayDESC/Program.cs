﻿namespace SortArrayDESC;

public class Program
{
    static void Main(string[] args)
    {
        var sorted = SortArrayDesc(new int[] { 1 });
        Console.WriteLine(sorted);
    }

    public static int[] SortArrayDesc(int[] x)
    {
        //kết quả hàm sau khi thực thi là mảng số nguyên mới đã sắp xếp theo thứ tự giảm dần
        int temp;
        for (int j = 0; j <= x.Length - 2; j++)
        {
            for (int i = 0; i <= x.Length - 2; i++)
            {
                if (x[i] < x[i + 1])
                {
                    temp = x[i + 1];
                    x[i + 1] = x[i];
                    x[i] = temp;
                }
            }
        }
        return x;
        throw new NotImplementedException();
    }

}