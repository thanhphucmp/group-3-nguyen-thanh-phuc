﻿namespace IndexOfSecondLargestElementInArray;

public class Program
{
    static void Main(string[] args)
    {
        var myArr = new[] { 1, 2, 3 };
        var result = IndexOfSecondLargestElementInArray(myArr);
        Console.WriteLine(result);
    }

    public static int IndexOfSecondLargestElementInArray(int[] x)
    {
        // kết quả hàm sau khi thực thi là vị trí(index) của số lớn thứ 2 trong mảng
        if (x.Length == 1 || x == null)
            return -1;

        int index = 0;
        int max1 = x[0];
        for (int i = 0; i < x.Length; i++)
        {
            if (x[i] > max1)
                max1 = x[i];
        }
        int max2 = 0;
        for (int i = 0; i < x.Length; i++)
        {
            if (x[i] > max2 && x[i] != max1)
            {
                max2 = x[i];
                index = i;
            }
        }
        return index;

        throw new NotImplementedException();
    }
}