﻿using System.Formats.Asn1;
using System.Security.Cryptography;

var result = PracticeStrings.MixTwoStrings("", "");
Console.WriteLine(result);
Console.WriteLine("hello world");

public class PracticeStrings
{

    public static string MixTwoStrings(string value1, string value2)
    {
        // xem file README.md
        string answer = "";
        for (int i = 0; i < int.Min(value1.Length, value2.Length); i++)
        {
            answer.Append(value1[i]);
            answer.Append(value2[i]);
        }
        for (int i = int.Min(value1.Length, value2.Length); i < int.Max(value1.Length, value2.Length); i++)
        {
            if (i < value1.Length)
                answer.Append(value1[i]);
            if (i < value2.Length)
                answer.Append(value2[i]);
        }


        return answer;
        throw new NotImplementedException();
    }

}

